import json

from django.apps import apps
from django.forms import model_to_dict, modelform_factory
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404


def model_index(request, model_label):
    model = _get_model(model_label)

    if request.method == 'GET':
        queryset = _apply_queryset_params(model, request.GET)
        return JsonResponse({
            'data': [model_to_dict(x) for x in queryset]
        })

    if request.method == 'POST':
        return _save_model_resp(request, model, None, 201)


def model_record(request, model_label, pk):
    model = _get_model(model_label)
    record = get_object_or_404(model, pk=pk)

    if request.method == 'DELETE':
        record.delete()
        return JsonResponse({})

    if request.method == 'PUT':
        return _save_model_resp(request, model, record, 200)


def _get_model(label):
    try:
        app, model = label.lower().split('.')
        return apps.all_models[app][model]
    except (ValueError, KeyError):
        raise Http404("Model isn't found")


def _apply_queryset_params(model, params):
    # If the model has a column with a special name ('offset', 'limit', etc),
    # you still may filter records by this column by adding '__exact' lookup modifier:
    # >> _apply_queryset_params(model, {'offset': 10, 'limit': 2, 'offset__exact': 2})

    ret = model.objects.all()
    for name, value in params.items():
        if name == 'offset':
            ret = ret[int(value):]
        elif name == 'limit':
            ret = ret[:int(value)]
        elif name == 'order_by':
            ret = ret.order_by(value)
        else:
            ret = ret.filter(**{name: value})
    return ret


def _save_model_resp(request, model, instance, success_status):
    form_cls = modelform_factory(model, fields='__all__')
    req_json = json.loads(request.body)
    form = form_cls(req_json, instance=instance)

    if form.is_valid():
        rec = form.save()
        return JsonResponse({'data': model_to_dict(rec)}, status=success_status)

    return JsonResponse({'errors': form.errors}, status=422)
