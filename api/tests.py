import json
from datetime import datetime

from django.forms import model_to_dict
from django.test import TestCase
from django.urls import reverse

from .models import Sample1, Sample2
from .views import model_index, model_record


def create_sample1(integer=123, char='abc'):
    ret = Sample1(integer=integer, char=char)
    ret.save()
    return ret


class TestModelIndex(TestCase):
    def test_it_returns_404_when_the_model_isnt_found_or_label_has_invalid_format(self):
        invalid_labels = ['foo', 'api.bar']

        for label in invalid_labels:
            resp = self.client.get(_model_index_url(label))
            self.assertEqual(resp.status_code, 404)

    def test_GET_returns_200_when_the_models_exists(self):
        resp = self.client.get(_model_index_url('api.sample1'))
        self.assertEqual(200, resp.status_code)

    def test_GET_returns_the_list_of_items(self):
        resp = self.client.get(_model_index_url('api.sample1'))
        self.assertEqual({'data': []},
                         json.loads(resp.content))

        rec1 = create_sample1()
        rec2 = create_sample1()

        resp = self.client.get(_model_index_url('api.sample1'))
        self.assertEqual(
            {'data': [model_to_dict(rec1),
                      model_to_dict(rec2)]},
            json.loads(resp.content)
        )

    def test_GET_considers_query_parameters_when_building_the_response(self):
        rec1 = create_sample1()
        rec2 = create_sample1(char='def', integer=0)
        rec3 = create_sample1(integer=0)

        url = _model_index_url('api.sample1') + '?char=abc&integer__gt=1'
        resp = self.client.get(url)
        self.assertEqual(
            {'data': [model_to_dict(rec1)]},
            json.loads(resp.content),
            "Invalid behavior when filtering the collection by multiple fields"
        )

        url = _model_index_url('api.sample1') + '?offset=1&limit=1'
        resp = self.client.get(url)
        self.assertEqual(
            {'data': [model_to_dict(rec2)]},
            json.loads(resp.content),
            "Invalid behavior when applying 'offset' and 'limit' parameters"
        )

        url = _model_index_url('api.sample1') + '?order_by=-id&limit=1'
        resp = self.client.get(url)
        self.assertEqual(
            {'data': [model_to_dict(rec3)]},
            json.loads(resp.content),
            "Invalid behavior when applying 'limit' and 'order_by' parameters"
        )

    def test_POST_creates_an_item(self):
        data = {'char': 'def', 'integer': 11}

        resp = self.client.post(
            _model_index_url('api.sample1'),
            json.dumps(data),
            content_type='application/json'
        )

        self.assertEqual(201, resp.status_code)

        record_data = {'id': 1, **data}
        self.assertEqual({'data': record_data}, json.loads(resp.content))
        self.assertEqual([record_data],
                         [model_to_dict(x) for x in Sample1.objects.all()])

    def test_POST_creates_an_item_with_foreign_key(self):
        rec = create_sample1()

        data = {'datetime': str(datetime.now()), 'foreign': 1}

        resp = self.client.post(
            _model_index_url('api.sample2'),
            json.dumps(data),
            content_type='application/json'
        )

        self.assertEqual(201, resp.status_code)
        self.assertEqual(rec, Sample2.objects.first().foreign)

    def test_POST_doesnt_create_an_item_when_data_is_invalid(self):
        resp = self.client.post(
            _model_index_url('api.sample1'),
            '{}',
            content_type='application/json'
        )

        self.assertEqual(0, Sample1.objects.count())

        self.assertEqual(422, resp.status_code)

        resp_json = json.loads(resp.content)
        self.assertIn('char', resp_json['errors'])
        self.assertIn('integer', resp_json['errors'])


class TestModelRecord(TestCase):
    def test_it_returns_404_when_the_model_isnt_found_or_label_has_invalid_format(self):
        invalid_labels = ['foo', 'api.bar']

        for label in invalid_labels:
            resp = self.client.get(_model_index_url(label))
            self.assertEqual(resp.status_code, 404)

    def test_it_returns_404_when_record_isnt_found(self):
        resp = self.client.delete(_model_record_url('api.sample1', 1))
        self.assertEqual(404, resp.status_code)

    def test_DELETE_removes_an_item(self):
        create_sample1()

        resp = self.client.delete(_model_record_url('api.sample1', 1))
        self.assertEqual(200, resp.status_code)
        self.assertEqual(0, Sample1.objects.count())

    def test_PUT_updates_an_item(self):
        rec = create_sample1(integer=1)

        resp = self.client.put(
            _model_record_url('api.sample1', rec.pk),
            json.dumps({'integer': 2, 'char': rec.char}),
            content_type='application/json'
        )

        self.assertEqual(200, resp.status_code)
        self.assertEqual(
            2,
            Sample1.objects.get(pk=rec.pk).integer,
        )

    def test_PUT_doesnt_update_an_item_when_data_isnt_valid(self):
        rec = create_sample1()

        resp = self.client.put(
            _model_record_url('api.sample1', rec.pk),
            json.dumps({'char': '', 'integer': 123}),
            content_type='application/json'
        )

        self.assertEqual(422, resp.status_code)
        self.assertEqual(
            rec.char,
            Sample1.objects.get(pk=rec.pk).char,
        )


def _model_index_url(model_label):
    return reverse(model_index, kwargs={'model_label': model_label})


def _model_record_url(model_label, pk):
    return reverse(model_record, kwargs={'model_label': model_label, 'pk': pk})
