import sys

from django.db import models

if 'test' in sys.argv or 'makemigrations' in sys.argv:
    # Following models should be available only while executing unit tests.
    # The condition that prevents the tables from being created in dev and prod environments
    # is located in ./migrations/0001_initial.py

    class Sample1(models.Model):
        integer = models.IntegerField()
        char = models.CharField(max_length=10)


    class Sample2(models.Model):
        foreign = models.ForeignKey(Sample1, on_delete=models.CASCADE)
        datetime = models.DateTimeField()
