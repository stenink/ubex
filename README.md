## Установка

```
git clone git@bitbucket.org:stenink/ubex.git
cd ubex
pip install -r requirements.txt
./manage.py migrate
```

## Примечания

Коммитил часто и таким образом, чтобы по коммитам вам было удобно отслеживать ход решения задачи.

Последний опыт работы с Django был давно, что-то могло уже забыться. Прошу сделать на это небольшую скидку.

## Если интересно протестировать с использованием CURL

Запускаем сервер:

```
./manage.py runserver
```

Для удобства, создаем обёртку над CURL:

```
export ubex='curl --cookie csrftoken=b5a081f56a3fe9ccdcdd11808637ca3b -H x-csrftoken:b5a081f56a3fe9ccdcdd11808637ca3b -i http://localhost:8000/api'
```

Отсылаем тестовый запрос на получение всех записей модели example.Post:

```
$ubex/example.Post/ 
```

Ответ:

```
HTTP/1.1 200 OK
Date: Tue, 11 Dec 2018 21:48:19 GMT
Server: WSGIServer/0.2 CPython/3.7.1
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Content-Length: 89

{"data": [{"id": 1, "title": "Example Title", "text": "Example text", "likes_count": 0}]}
```

Шлем запрос для создания новой записи:

```
$ubex/example.Post/ -X POST -d '{"title": "some title", "text": "some text", "likes_count": 2}'
```

Ответ:

```
HTTP/1.1 201 Created
Date: Tue, 11 Dec 2018 21:51:54 GMT
Server: WSGIServer/0.2 CPython/3.7.1
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Content-Length: 81

{"data": {"id": 2, "title": "some title", "text": "some text", "likes_count": 2}}
```

Тестируем возможность фильтрации данных при запросе списка записей:

```
$ubex/example.Post/?likes_count__gte=2
```

Ответ:

```
HTTP/1.1 200 OK
Date: Tue, 11 Dec 2018 21:57:47 GMT
Server: WSGIServer/0.2 CPython/3.7.1
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Content-Length: 83

{"data": [{"id": 2, "title": "some title", "text": "some text", "likes_count": 2}]}
```


Тестируем удаление записей:

```
$ubex/example.Post/1/ -X DELETE
```

Ответ:
```
HTTP/1.1 200 OK
Date: Tue, 11 Dec 2018 22:01:27 GMT
Server: WSGIServer/0.2 CPython/3.7.1
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Content-Length: 2

{}
```